#pragma once

#include <vector>
#include "SFML/Graphics.hpp"
#include "string"

class OptionsMenu
{
public:
	OptionsMenu();
	~OptionsMenu();
	void draw(sf::RenderWindow& window) const;
	std::vector<sf::Text> opList;
	sf::Font font;
	
};