#include "OptionsMenu.hpp"
#include "iostream"
#include "string"

OptionsMenu::OptionsMenu()
{
	font.loadFromFile("FeFCrm2.ttf");
	
	sf::Text text("hello", font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color::Red);
	
	opList.resize(1);

	opList[0] = text;
}

void OptionsMenu::draw(sf::RenderWindow& window) const
{
	window.draw(opList[0]);
}

OptionsMenu::~OptionsMenu()
{

}