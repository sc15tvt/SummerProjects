#include "main.hpp"

int main(int argc, char const *argv[])
{
	Game game(hight, width, mines);

	viewSize = game.getViewSize();

	sf::RenderWindow window(sf::VideoMode(400, 600), "Minesweeper", sf::Style::Close);

	window.setKeyRepeatEnabled(false);
	window.setFramerateLimit(30);

    // ajustes the view to my window to fit the grid
    view.reset(sf::FloatRect(0, 0, viewSize.x, viewSize.y));
    window.setSize(sf::Vector2u(viewSize.x, viewSize.y));

    window.setView(view);

    while(window.isOpen())
    {
    	// Event processing
	    sf::Event event;
	    while (window.pollEvent(event))
	    {
	        // Request for closing the window
	        if (event.type == sf::Event::Closed)
	        {
	        	window.close();
	        }

	        if(!game.loose)
	        {
	        	if(event.type == sf::Event::MouseButtonPressed)
	        	game.eventHandler(event);
	        	if(game.gameEnd())
	        	{
	        		/*
	        			Use for win condition
	        			Move outside the event loop
	        			Give the player a reward
	        		*/
	        	}

	        	if(event.type == sf::Event::KeyPressed)
	        	{
	        		if(event.key.code == sf::Keyboard::R)
	        		{
	        			game.restartGame();
	        			viewSize = game.getViewSize();

	        			view.reset(sf::FloatRect(0, 0, viewSize.x, viewSize.y));
    					window.setSize(sf::Vector2u(viewSize.x, viewSize.y));

    					window.setView(view);

    					game.loose = false;
	        		}
	        		else if(event.key.code == sf::Keyboard::Escape)
	        		{

	        		}
	        	}
	        }
	    }

	    //handels screen rendering dont mess with for now
	    // set backgound colour 
	    window.clear(sf::Color(0, 0, 0, 255));

	   	game.draw(window);
	   	optionsMenu.draw(window);
		window.display();

    }

	return 0;
}

